import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#006400',
    textAlign: 'center',
    padding: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
  },
});

export default Header;
