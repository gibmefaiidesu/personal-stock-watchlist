import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';

class Item extends Component {
  render() {
    return (
      <View style={styles.item_card}>
        <Text style={styles.item_name}>{this.props.data.name}</Text>
        <View style={styles.subcontent}>
          <View style={styles.price_display}>
            <Text style={styles.intrinsic_value}>
              {this.props.data.intrinsic_value}
            </Text>
          </View>
          <View style={styles.price_display}>
            <Text style={styles.price}>{this.props.data.price}</Text>
          </View>
        </View>
      </View>
    );
  }
}

class List extends Component {
  render() {
    return (
      <FlatList
        data={this.props.data}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => <Item data={item} />}
      />
    );
  }
}

const styles = StyleSheet.create({
  item_card: {
    backgroundColor: '#8fbc8f',
    padding: 10,
    marginVertical: 5,
  },
  item_name: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white',
  },
  subcontent: {
    flex: 6,
    flexDirection: 'row',
  },
  price_display: {
    flex: 3,
  },
  intrinsic_value: {
    textAlign: 'center',
  },
  price: {
    textAlign: 'center',
  },
});

export default List;
